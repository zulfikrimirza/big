public class BinarySearch
{
  int[] arr;
  
  public BinarySearch(int[] arr)
  {
    this.arr = arr;
  }

  public int binarySearch(int x, int l, int r)
  {
    while(l <= r){
      int mid = l + (r-l) / 2;
      if(x == arr[mid]) return mid;
      else if (x < arr[mid]){
        r = mid - 1;
      }
      else {
        l = mid + 1;
      }
    }
    return -1;
  }
}
