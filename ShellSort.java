public class ShellSort
{
  int arr[];

  public ShellSort(int[] arr)
  {
    this.arr = arr;
    shellSort(arr);
  }

  public void shellSort(int[] arr)
  {
    int gap = arr.length / 2;
    while (gap > 0){
      int j =0;
      for(int i=gap; i<arr.length; i++){
        int temp = arr[i];
        for(j=i; j>=gap && arr[j-gap] > temp; j-=gap){
          arr[j] = arr[j-gap];
        }
        arr[j] = temp;
      }
      gap = gap / 2;
    }
  }

  public int[] getArray()
  {
    return arr;
  }
}
