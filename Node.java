/*Rahagi Ahnaf Saidani
 * 1177050094
 * UIN SGD BANDUNG
 */

public class Node
{
  String data;
  Node next;

  public Node(String data)
  {
    this.data = data;
  }

  public void tampil(int counter)
  {
    System.out.println(counter + "." + data);
  }
}
