import java.util.Scanner;
import java.util.Hashtable;
import java.util.Random;

public class Main
{
  static Scanner inpt = new Scanner(System.in);
  public static void main(String[] args){
    printBaris(65);
    System.out.println("1.Mengukur Waktu Algoritma (Sort & Search)");
    System.out.println("2.Input Mahasiswa (Linked List)");
    System.out.println("3.Input NIM Mahasiswa Terurut (Binary Tree)");
    System.out.println("4.Kamus Tentang Informatika (Hash Table)");
    System.out.println("5.Exit");
    printBaris(65);
    System.out.print("Masukkan pilihan : ");
    char pil = inpt.next().charAt(0);
    inpt.nextLine();
    printBaris(35);
    switch(pil){
      case '1':
        //
        //SEARCH & SORT
        //
        System.out.println("Buat Array Integer Random");
        printBaris(35);
        System.out.print("Masukkan banyak data : ");
        int arrLen = inpt.nextInt();
        int[] arr = new int[arrLen];
        System.out.print("Masukkan nilai max random : ");
        int max = inpt.nextInt();
        printBaris(35);
        buatArray(arr, max);
        System.out.println("Buat Array Selesai");
        printBaris(35);
        System.out.println("1.Sort Array");
        System.out.println("2.Search Array");
        printBaris(35);
        System.out.print("Masukkan pilihan : ");
        char pilArr = inpt.next().charAt(0);
        printBaris(35);

        if(pilArr == '1'){
          System.out.println("Pilih Algoritma");
          System.out.println("1.Quick Sort");
          System.out.println("2.Shell Sort");
          printBaris(35);
          System.out.print("Masukkan pilihan : ");
          char pilSort = inpt.next().charAt(0);
          inpt.nextLine();
          if(pilSort == '1'){
            double startTime = System.nanoTime();
            QuickSort qs = new QuickSort(arr);
            double endTime = System.nanoTime();
            double durasi = (endTime - startTime) / 1000000;
            System.out.println("Array sesudah di sort : ");
            printBaris(35);
            printArray(arr);
            printBaris(35);
            System.out.println("Sorting diselesaikan dalam waktu : " + durasi + "ms");
          }
          else if(pilSort == '2'){
            double startTime = System.nanoTime();
            ShellSort ss = new ShellSort(arr);
            double endTime = System.nanoTime();
            double durasi = (endTime - startTime) / 1000000;
            System.out.println("Array sesudah di sort : ");
            printArray(arr);
            printBaris(35);
            System.out.println("Sorting diselesaikan dalam waktu : " + durasi + "ms");
          }
        }
        else if(pilArr == '2'){
          System.out.println("Pilih Algoritma");
          System.out.println("1.Linear Search");
          System.out.println("2.Binary Search (Data Terurut)");
          printBaris(35);
          System.out.print("Masukkan pilihan : ");
          char pilSearch = inpt.next().charAt(0);
          printBaris(35);
          inpt.nextLine();
          if(pilSearch == '1'){
            LinearSearch ls = new LinearSearch(arr);
            System.out.print("Urut data (y/n) : ");
            char ynLinSearch = inpt.next().charAt(0);
            printBaris(35);
            inpt.nextLine();
            if(ynLinSearch == 'y'){
              QuickSort qs = new QuickSort(arr);
            }
            int hasilLinearSearch = 0;
            do{
              System.out.print("Masukkan data yang dicari : ");
              int cariLin = inpt.nextInt();
              double startTime = System.nanoTime();
              hasilLinearSearch = ls.linearSearch(arr, cariLin);
              double endTime = System.nanoTime();
              double durasi = (endTime - startTime) / 1000000;
              if(hasilLinearSearch != -1){
                System.out.println("Data ada di indeks ke - " + hasilLinearSearch);
                System.out.println("Pencarian dilakukan dalam waktu : " + durasi + "ms");
              }
              else{
                System.out.println("Data tidak ditemukan, lakukan pencarian kembali :)");
              }
            }while(hasilLinearSearch == -1);
          }
          else if (pilSearch == '2'){
            QuickSort qs = new QuickSort(arr);
            BinarySearch bs = new BinarySearch(arr);
            int hasilBinarySearch = 0;
            do{
              System.out.print("Masukkan data yang dicari : ");
              int cariBin = inpt.nextInt();
              double startTime = System.nanoTime();
              hasilBinarySearch = bs.binarySearch(cariBin, 0, arr.length-1);
              double endTime = System.nanoTime();
              double durasi = (endTime - startTime) / 1000000;
              if(hasilBinarySearch != -1){
                System.out.println("Data ada di indeks ke - " + hasilBinarySearch);
                System.out.println("Pencarian dilakukan dalam waktu : " + durasi + "ms");
              }
              else{
                System.out.println("Data tidak ditemukan, lakukan pencarian kembali :)");
              }
            }while(hasilBinarySearch == -1);

          }
        }
          break;
      case '2':
          //
          //LINKED LIST
          //
          LinkedList link = new LinkedList();
          link.addLast("Rahagi");
          link.addLast("Zaenal");
          link.addLast("Nurul");
          link.addLast("Yusuf");
          String namaMhs;
          System.out.println("1.Tambahkan data mahasiswa");
          System.out.println("2.Tampilkan data mahasiswa");
          System.out.println("3.Urutkan data mahasiswa");
          printBaris(35);
          System.out.print("Masukkan pilihan : ");
          char pilLink = inpt.next().charAt(0);
          inpt.nextLine();
          printBaris(35);
          //INPUTAN LINKED LIST
          if(pilLink == '1'){
            do{
              System.out.print("Masukkan nama mahasiswa (masukkan quit jika sudah selesai) : ");
              namaMhs = inpt.nextLine();
              if(!(namaMhs.toLowerCase().equals("quit"))){
                link.addLast(namaMhs);
              }
              else{

              }
            }while(!(namaMhs.equals("quit")));
            printBaris(35);
            System.out.print("Tampilkan data mahasiswa ? (y/n) ");
            char ynLink = inpt.next().charAt(0);
            inpt.nextLine();
            if(ynLink == 'y'){
              System.out.print("Sort data maha siswa ? (y/n) ");
              char ynLinkSort = inpt.next().charAt(0);
              inpt.nextLine();
              printBaris(35);
              if(ynLinkSort == 'y'){
                link.sortList();
                link.printList();
              }
              else {
                link.printList();
              }
            }
            else{

            }

          }
          //INPUTAN LINKED LIST

          //PRINT LINKED LIST
          else if(pilLink == '2'){
            printBaris(35);
            link.printList();
          }
          //PRINT LINKED LIST

          //SORT LINKED LIST
          else {
            link.sortList();
            printBaris(35);
            link.printList();
          }
          //SORT LINKED LIST
          break;
      case '3':
          //
          //TREE
          //
          BinarySearchTree bst = new BinarySearchTree();
          String nimMhs = "";

          while (!(nimMhs.equals("quit"))){
            System.out.print("Masukkan NIM mahasiswa (masukkan 'quit' jika sudah selesai) : ");
            nimMhs = inpt.nextLine();
            if (nimMhs.equals("quit")){
              break;
            }
            else {
              bst.insert(nimMhs);
            }
          }
          System.out.print("Tampilkan data terurut ? (y/n) : ");
          char ynBst = inpt.next().charAt(0);
          inpt.nextLine();
            
          printBaris(35);
          if(ynBst == 'y'){
            bst.printInOrder();
          }
          else{
            System.out.println("Selesai");
          }
          break;
      case '4':
          //
          //HASH TABLE
          //
          Hashtable<String, String> kamus = new Hashtable<String, String>();
          kamus.put("search engine", "Software sistem yang dirancang untuk mencari informasi di internet.");
          kamus.put("download", "Menyalin file dari internet ke dalam media penyimpanan lokal");
          kamus.put("compiling", "Proses penerjemahan ke bahasa mesin.");
          kamus.put("file", "Komputer bergerak yang berukuran relatif kecil dan ringan.");
          kamus.put("microcomputer", "Adalah sebuah kelas komputer yang menggunakan mikroprosesor sebagai CPU utamanya.");
          kamus.put("input device", "Semua periferal (perangkat keras komputer) yang digunakan untuk memberikan data dan sinyal kendali untuk suatu sistem pemrosesan informasi.");
          kamus.put("output device", "Adalah peranti yang dapat menampilkan hasil pengolahan, hasil pemasukan data atau perintah pada komputer.");
          kamus.put("fiber optic", "Saluran transmisi atau sejenis kabel yang terbuat dari kaca atau plastik yang sangat halus dan lebih kecil dari sehelai rambut, dan dapat digunakan untuk mentransmisikan sinyal cahaya dari suatu tempat ke tempat lain.");
          kamus.put("server", "Merupakan sebuah sistem komputer yang menyediakan jenis layanan tertentu dalam sebuah jaringan komputer.");
          kamus.put("algoritma", "Adalah prosedur langkah-demi-langkah untuk penghitungan. Algoritme digunakan untuk penghitungan, pemrosesan data, dan penalaran otomatis. ");

          System.out.println("1.Tambahkan kata pada kamus");
          System.out.println("2.Cari arti kata");
          printBaris(35);
          System.out.print("Masukkan pilihan : ");
          char pilHash = inpt.next().charAt(0);
          inpt.nextLine();
          printBaris(35);
          
          if (pilHash == '1'){
            System.out.print("Masukkan kata : ");
            String key = inpt.nextLine();
            System.out.print("Masukkan arti : ");
            String value = inpt.nextLine();

            kamus.put(key, value);
            printBaris(35);
            System.out.println("Data berhasil ditambahkan.");
            System.out.print("Tampilkan data ? (y/n) : ");
            char ynHash = inpt.next().charAt(0);
            inpt.nextLine();
            if(ynHash == 'y'){
              System.out.println(key + " : " + kamus.get(key));
            }
          }
          else if (pilHash == '2'){
            System.out.print("Cari : ");
            String cariHash = inpt.nextLine().toLowerCase();
            printBaris(35);
            System.out.println("Hasil Pencarian : ");
            if(kamus.containsKey(cariHash)){
              System.out.println(kamus.get(cariHash));
            }
            else {
              System.out.println("Tidak ketemu : (");
            }
          }
          break;
      default:

        }
    printBaris(35);
  }

  private static void buatArray(int[] arr, int max)
  {
    Random r = new Random();
    for(int i=0; i<arr.length; i++){
      arr[i] = r.nextInt(max);
    }
  }

  private static void printArray(int[] arr)
  {
    for(int i=0; i<arr.length; i++){
      System.out.print(arr[i] + " ");
    }
    System.out.println("");
  }

  private static void printBaris(int n)
  {
    for(int i=0; i<n; i++){
      System.out.print("=");
    }
    System.out.println("");
  }
}
